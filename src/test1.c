/*
 ===============================================================================
 Name        : test1.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
 ===============================================================================
 */

#ifdef __USE_CMSIS
#include "LPC43xx.h"
#endif

#include <cr_section_macros.h>

#if defined (__MULTICORE_MASTER_SLAVE_M0APP) | defined (__MULTICORE_MASTER_SLAVE_M0SUB)
#include "cr_start_m0.h"
#endif

#include <string.h>
#include "arm_math.h"
#include "arm_const_structs.h"

#include "intercom.h"

extern uint32_t SystemCoreClock;

intercom_t __attribute__ ((section(".bss.$RamAHB16"))) intercom;	///< Intercom structure at fixed memory location

#define SHIFT_BLOCK_BITS              (4)
void shift_block(uint32_t* block);

#define INPUT_BLOCK_SIZE       (2*INTERCOM_BUFFER_SIZE)
#define FFT_OUT_BLOCK_SIZE     (INPUT_BLOCK_SIZE*2)
#define MAGNITUDE_BLOCK_SIZE   (INPUT_BLOCK_SIZE/2)
q15_t buffer0[INPUT_BLOCK_SIZE];	//First input ping-pong buffer
q15_t buffer1[INPUT_BLOCK_SIZE];	//Second input ping-pong buffer
q15_t fft_out0[FFT_OUT_BLOCK_SIZE];	//Output of the FFT, complex numbers (only first half is interesting)
q15_t fft_out1[FFT_OUT_BLOCK_SIZE];	//Output of the FFT, complex numbers (only first half is interesting)
q15_t magnitude0[MAGNITUDE_BLOCK_SIZE];	//Magnitude squared after FFT
q15_t magnitude1[MAGNITUDE_BLOCK_SIZE];	//Magnitude squared after FFT
uint32_t magnitude_buffer_switch;	//Set to switch between magnitude 0 and 1
arm_rfft_instance_q15 rfft_instance;	//Real FFT instance

/**
 * @brief Main program function.
 * @return doesn't return
 */
int main(void)
{
#if 0
	volatile uint32_t shit = 1;
	while(shit);
#endif

	intercom.ptr_SystemCoreClock = &SystemCoreClock;	//Point to SystemCoreClock for other cores
	intercom.buffer0 = (uint32_t*)buffer0;	//Point to buffers where DMA writes AD values
	intercom.buffer1 = (uint32_t*)buffer1;
	intercom.fft_out = (int16_t*)fft_out0;	//Point to buffers where FFT results are stored
	intercom.mag = (int16_t*)magnitude0;
	intercom.mag_last = (int16_t*)magnitude1;
	magnitude_buffer_switch = 0;
	arm_rfft_init_q15(&rfft_instance, 128, 0, 1);	//Initialize rfft

	// Start M0APP slave processor
#if defined (__MULTICORE_MASTER_SLAVE_M0APP)
	cr_start_m0(SLAVE_M0APP, &__core_m0app_START__);
#endif

	// Start M0SUB slave processor
#if defined (__MULTICORE_MASTER_SLAVE_M0SUB)
	cr_start_m0(SLAVE_M0SUB,&__core_m0sub_START__);
#endif

	NVIC_EnableIRQ(M0_DMA_IRQn);	//Enable interrupt for block read

	while(1)
	{
		__NOP();
	}

	return 0;
}

/**
 * @brief DMA handler for page read done and DMA error.
 */
void DMA_IRQHandler()
{
	if(LPC_GPDMA->INTTCSTAT & 0x1)	//Transfer complete
	{
		LPC_GPDMA->INTTCCLEAR = 0x1;	//Clear the interrupt

		if(LPC_GPDMA->C0LLI != intercom.lli0_address)	//First buffer is being written
		{
			shift_block((uint32_t*)buffer1);	//Multiply to fit q15 range
			arm_rfft_q15(&rfft_instance, buffer1, fft_out1);	//Calculate FFT
			magnitude_buffer_switch ^= 0x1;	//Switch magnitude buffers
			if(magnitude_buffer_switch)
			{
				arm_cmplx_mag_squared_q15(&fft_out1[0],
						&magnitude1[0], MAGNITUDE_BLOCK_SIZE/2);	//Get magnitude^2
				intercom.mag = magnitude1;
				intercom.mag_last = magnitude0;
			}
			else
			{
				arm_cmplx_mag_squared_q15(&fft_out1[0],
						&magnitude0[0], MAGNITUDE_BLOCK_SIZE/2);	//Get magnitude^2
				intercom.mag = magnitude0;
				intercom.mag_last = magnitude1;
			}
			intercom.mag_channels = 0;	//Set channel offset
			intercom.fft_out = fft_out1;	//Point to finished data
			__SEV();	//Send interrupt to M0 core
		}
		else	//Second buffer is being written
		{
			shift_block((uint32_t*)buffer0);	//Multiply to fit q15 range
			arm_rfft_q15(&rfft_instance, buffer0, fft_out0);	//Calculate FFT
			if(magnitude_buffer_switch)
			{
				arm_cmplx_mag_squared_q15(&fft_out0[(2*MAGNITUDE_BLOCK_SIZE)/2],
						&magnitude1[MAGNITUDE_BLOCK_SIZE/2], MAGNITUDE_BLOCK_SIZE/2);	//Get magnitude^2
				intercom.mag = magnitude1;
				intercom.mag_last = magnitude0;
			}
			else
			{
				arm_cmplx_mag_squared_q15(&fft_out0[(2*MAGNITUDE_BLOCK_SIZE)/2],
						&magnitude0[MAGNITUDE_BLOCK_SIZE/2], MAGNITUDE_BLOCK_SIZE/2);	//Get magnitude^2
				intercom.mag = magnitude0;
				intercom.mag_last = magnitude1;
			}
			intercom.mag_channels = MAGNITUDE_BLOCK_SIZE/2;	//Set channel offset
			intercom.fft_out = fft_out0;	//Point to finished data
			__SEV();	//Send interrupt to M0 core
		}
	}
	if(LPC_GPDMA->INTERRSTAT & 0x1)	//DMA error
	{
		LPC_GPDMA->INTERRCLR = 0x1;	//Clear the interrupt
	}
}

/**
 * @brief Shift block of data by 4 bits left.
 * @param block 128 x q15_t elements of data
 */
void shift_block(uint32_t* block)
{
	uint32_t i;
	for(i = 0; i < 64; i += 4)
	{
		block[i  ] <<= SHIFT_BLOCK_BITS;
		block[i+1] <<= SHIFT_BLOCK_BITS;
		block[i+2] <<= SHIFT_BLOCK_BITS;
		block[i+3] <<= SHIFT_BLOCK_BITS;
	}
}
